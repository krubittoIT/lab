package Services;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;
import contracts.ITransport;
import entity.Automobile;
import errors.NoSuchModelNameException;

public class Transport {

    public static void showList(final ITransport transport) {
        final int size = transport.getLength();
        final String[] arrName = transport.getNames();
        final double[] arrPrice = transport.getPrices();

        for (int i = 0; i < size; i++)
            System.out.println("модель " + arrName[i] + " с ценой " + arrPrice[i]);
    }

    public static float getAvg(final ITransport transport) {
        final int size = transport.getLength();
        final double[] arrPrice = transport.getPrices();
        float avg = 0;

        for (final double item : arrPrice)
            avg += item;

        return avg / size;
    }

    // записи информации о транспортном средстве в байтовый поток
    // марку т/c, количество моделей, а затем список моделей и цен моделей
    public static void outputTransport(final ITransport v, final OutputStream out) throws IOException {
        byte[] bait;
        final DataOutputStream stream = new DataOutputStream(out);

        bait = v.getMark().getBytes();
        stream.writeInt(bait.length);
        for (int i = 0; i < bait.length; i++) {
            stream.writeByte(bait[i]);
        }
        final int count = v.getLength();
        stream.writeInt(count);// Количество моделей
        // Список моделей
        String[] models = new String[v.getLength()];
        models = v.getNames();
        // Список цен
        double[] prices = new double[v.getLength()];
        prices = v.getPrices();
        for (int i = 0; i < count; i++) {
            bait = models[i].getBytes();
            stream.writeInt(bait.length);
            for (int j = 0; j < bait.length; j++) {
                stream.writeByte(bait[j]);
            }
            stream.writeDouble(prices[i]);
        }
    }

    // чтения информации о транспортном средстве из байтового потока
    public static ITransport inputTransport(final InputStream in) throws IOException, NoSuchModelNameException {
        int l;
        ITransport result;
        final DataInputStream stream = new DataInputStream(in);
        l = stream.readInt();
        byte[] bait = new byte[l];
        for (int i = 0; i < l; i++) {
            bait[i] = stream.readByte();
        }
        final String mark = new String(bait);// марка

        final int count = stream.readInt();// кол моделей
        result = new Automobile(mark, count);
        // Список моделей
        String[] models = result.getNames();

        for (int i = 0; i < count; i++) {
            l = stream.readInt();
            bait = new byte[l];
            for (int j = 0; j < l; j++) {
                bait[j] = stream.readByte();
            }
            String model = new String(bait);
            double price = stream.readDouble();
            result.setModel(models[i], model);
            result.setPrice(model, price);
        }

        return result;
    }

    // записи информации о транспортном средстве в символьный поток
    public static void writeTransport(final ITransport v, final Writer out) throws IOException {
        PrintWriter pw = new PrintWriter(out);
        // марка
        pw.println(v.getMark());
        // кол-во моделей
        pw.println(v.getLength());
        // список моделей
        String[] models = v.getNames();
        // список цен
        double[] prices = v.getPrices();
        for (int i = 0; i < v.getLength(); i++) {
            pw.println(models[i]);
            pw.println(prices[i]);
        }
        out.flush();// финализирует выходное состояние, очищая все буферы вывода
    }

    // чтения информации о транспортном средстве из символьного потока
    public static ITransport readTransport(final Reader in) throws IOException, NoSuchModelNameException {
        BufferedReader br = new BufferedReader(in);
        String mark = br.readLine();
        int countModel = Integer.parseInt(br.readLine());
        ITransport p = new Automobile(mark, countModel);
        // Список моделей
        String[] models = p.getNames();
        for (int i = 0; i < countModel; i++) {
            String model = br.readLine();
            double price = Double.parseDouble(br.readLine());
            p.setModel(models[i], model);
            p.setPrice(model, price);
        }

        br.close();
        return p;
    }

    public static ITransport createTransport(String nameMark, int sizeModel, ITransport type){
        try {
            Class clas = type.getClass();
            Constructor constr = clas.getDeclaredConstructor(String.class, int.class);
            Object obj = null;
            obj = constr.newInstance(nameMark, sizeModel);
            return (ITransport) obj;
        } catch (Exception e) {
            return null;
        }
    }

    public static double getAvgPer(ITransport... transport) {
        double sum = 0;
        for (int i = 0; i < transport.length; i++) {
            sum += getAvg(transport[i]);
        }
        return sum / transport.length;
    }

    public static void writeTransportPrintf(ITransport v, Writer w) throws IOException {
        final int count = v.getLength();

        PrintWriter newTransport = new PrintWriter(w);
        newTransport.printf("%d", count);
        newTransport.println();
        newTransport.printf("%s", v.getMark());
        newTransport.println();

        // Список моделей
        String[] models = new String[count];
        models = v.getNames();
        // Список цен
        double[] prices = new double[count];
        prices = v.getPrices();

        for (int i = 0; i < count; i++) {
            newTransport.printf("%s", models[i]);
            newTransport.println();
            newTransport.printf("%f", prices[i]);
            newTransport.println();
        }
    }

    public static ITransport readTransportScanner(Reader in) throws IOException, NoSuchModelNameException {
        Scanner sc = new Scanner(in);
        int size = sc.nextInt();
        sc.nextLine();
        ITransport result = new Automobile(sc.nextLine(), size);
        // Список моделей
        String[] models = result.getNames();
        for (int i = 0; i < size; i++) {
            String model = sc.nextLine();
            double price = sc.nextDouble();
            result.setModel(models[i], model);
            result.setPrice(model, price);
            models = result.getNames();
            sc.nextLine();
        }
        return result;
    }

}
