package contracts;

import errors.DuplicateModelNameException;
import errors.NoSuchModelNameException;

public interface ITransport {

    public String getMark();

    public void setMark(String mark);

    public void setModel(String oldName, String newName) throws NoSuchModelNameException;

    public String[] getNames();

    public double getPrice(String name) throws NoSuchModelNameException;

    public void setPrice(String name, double newPrice) throws NoSuchModelNameException;

    public double[] getPrices();

    public void add(String name, double price) throws DuplicateModelNameException;

    public void delete(String name, double price) throws NoSuchModelNameException;

    public int getLength();
}
