package laba;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

import contracts.ITransport;
import entity.*;
import errors.DuplicateModelNameException;
import errors.NoSuchModelNameException;
import threads.BasicThread.*;
import threads.ExecutorThread.ExecutRunnable;
import threads.RunnableLockThread.*;
import threads.RunnableThread.*;
import threads.RunnableThread.Queue.*;
import threads.model.TransportSynchronizer;

public class LabSix {

    public static void taskOne() throws DuplicateModelNameException, NoSuchModelNameException, InterruptedException {
        System.out.println("----");
        System.out.println("Задание 1:");
        System.out.println("----");
        var automobile = getDefaultTransport();
        PriceThread pt = new PriceThread(automobile);
        pt.setPriority(Thread.MAX_PRIORITY);

        ModelsThread mt = new ModelsThread(automobile);
        mt.setPriority(Thread.MIN_PRIORITY);

        pt.start();
        mt.start();
    }

    public static void taskTwo() throws DuplicateModelNameException, NoSuchModelNameException, InterruptedException {
        System.out.println("----");
        System.out.println("Задание 2: ");
        System.out.println("----");
        var automobile = getDefaultTransport();
        TransportSynchronizer ts = new TransportSynchronizer(automobile);
        Runnable runOne = new PriceRunnable(ts);
        Thread threadOne = new Thread(runOne);
        Runnable runTwo = new ModelsRunnable(ts);
        Thread threadTwo = new Thread(runTwo);
        threadOne.start();
        threadTwo.start();
    }

    public static void taskThree() throws DuplicateModelNameException, NoSuchModelNameException, InterruptedException {
        System.out.println("----");
        System.out.println("Задание 3:");
        System.out.println("----");
        var automobile = getDefaultTransport();
        ReentrantLock reentrantLock = new ReentrantLock();
        Runnable runOneLock = new ModelsRunnableLock(automobile, reentrantLock);
        Thread threadOneLock = new Thread(runOneLock);
        Runnable runTwoLock = new PriceRunnableLock(automobile, reentrantLock);
        Thread threadTwoLock = new Thread(runTwoLock);
        threadTwoLock.start();
        threadOneLock.start();
        
    }

    public static void taskFour() throws DuplicateModelNameException, NoSuchModelNameException, InterruptedException {
        System.out.println("----");
        System.out.println("Задание 4:");
        System.out.println("----");
        ITransport transportOne = new Automobile("lada", 2);
        ITransport transportTwo = new Moped("henda", 2);
        ITransport transportThree = new Scooter("audi", 2);
        ITransport transportFour = new Atv("shkoda", 2);
        ITransport[] transportArr = new ITransport[] { transportOne, transportTwo, transportThree, transportFour };
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        for (ITransport transport : transportArr) {
            Runnable r = new ExecutRunnable(transport);
            executorService.submit(r);
        }
        executorService.shutdown();
    }

    public static void taskFive() throws DuplicateModelNameException, NoSuchModelNameException, InterruptedException {
        System.out.println("----");
        System.out.println("Задание 5:");
        System.out.println("----");

        String[] arrFileName = new String[] { "smarkFiles/tr_one.txt", "smarkFiles/tr_two.txt",
                "smarkFiles/tr_three.txt", "smarkFiles/tr_four.txt", "smarkFiles/tr_five.txt" };
        ArrayBlockingQueue<ITransport> blockingQueue = new ArrayBlockingQueue<ITransport>(2);

        //new Thread(new TransportReaderRunnable(blockingQueue)).start();

        for (String fileName : arrFileName) {
            new Thread(new TransportCreatorRunnable(fileName, blockingQueue)).start();
         }

        for (String fileName : arrFileName) {
        ITransport ff = blockingQueue.take();
        System.out.println(ff.toString());
         }
    }

    private static ITransport getDefaultTransport()
            throws DuplicateModelNameException, NoSuchModelNameException, InterruptedException {
        ITransport automobile = new Automobile("Lada", 20);
        automobile.add("xray", 100);
        automobile.setPrice("none-1", 100.10);
        automobile.setModel("none-0", "new-none");
        //System.out.println(automobile.toString());
        return automobile;
    }

}
