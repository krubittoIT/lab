package laba;

import Services.Transport;
import contracts.ITransport;
import entity.Automobile;
import entity.Motorcycle;
import errors.DuplicateModelNameException;
import errors.NoSuchModelNameException;

public class LabTwo {

    public static void doLabTwo() throws DuplicateModelNameException, NoSuchModelNameException {

        Motorcycle cycle = new Motorcycle("Lada", 2);

        System.out.println();
        System.out.println("Марка мотоцикла: " + cycle.getMark());
        System.out.println();

        System.out.println("Изменение марки мотоцикла на 'Lada v8'");
        cycle.setMark("Lada v8");
        System.out.println("Новая марка мотоцикла: " + cycle.getMark());
        System.out.println();

        System.out.println("Добавление моделей мотоциклу");
        cycle.add("vesta", 50);
        cycle.add("xray", 100);
        System.out.println("Вывод информации по моделям мотоцикла");
        printStringArrayToConsole(cycle.getNames());
        printDoubleArrayToConsole(cycle.getPrices());
        System.out.println();
        System.out.println();

        System.out.println("В базе сейчас " + cycle.getLength() + " моделей авто для марки " + cycle.getMark());
        System.out.println("Удаляем модель 'cycle-0' с прайсом 0");
        cycle.delete("cycle-0", 0);
        System.out.println("Теперь в базе сейчас " + cycle.getLength() + " моделей авто для марки " + cycle.getMark());
        System.out.println("Вывод информации по этим маркам мотоциклов");
        printStringArrayToConsole(cycle.getNames());
        printDoubleArrayToConsole(cycle.getPrices());
        System.out.println();
        System.out.println();

        System.out.println("Стоимость модели 'xray': " + cycle.getPrice("xray"));
        System.out.println("Изменяем стоимость модели 'xray' на 200");
        cycle.setPrice("xray", 200);
        System.out.println("Новая стоимость модели 'xray': " + cycle.getPrice("xray"));
        System.out.println();

        System.out.println("Изменям название модели 'vesta' на 'honda'");
        cycle.setModel("vesta", "honda");
        System.out.println("Вывод информации по маркам");
        printStringArrayToConsole(cycle.getNames());
        System.out.println();
        System.out.println();

        System.out.println("--------");
        printPeaople();
        System.out.println("--------");

        ITransport automobile = new Automobile("Lada", 2);

        System.out.println("марка авто: " + automobile.getMark());
        System.out.println();

        System.out.println("Изменение марки авто");
        automobile.setMark("Lada v2");
        System.out.println("Новая марка авто: " + automobile.getMark());
        System.out.println();
        System.out.println("Добавление марок");
        automobile.add("priora", 1000);
        automobile.add("4x4", 4000);
        automobile.add("kalina", 2000);
        System.out.println("Вывод информации по маркам");
        printStringArrayToConsole(automobile.getNames());
        printDoubleArrayToConsole(automobile.getPrices());
        System.out.println();
        System.out.println();

        System.out
                .println("В базе сейчас " + automobile.getLength() + " моделей авто для марки " + automobile.getMark());
        System.out.println("Удаляем модель 'Lada-0' с прайсом 0");
        automobile.delete("Lada-0", 0);
        System.out.println(
                "Теперь в базе сейчас " + automobile.getLength() + " моделей авто для марки " + automobile.getMark());
        System.out.println("Вывод информации по этим маркам маркам мотоцикла");
        printStringArrayToConsole(cycle.getNames());
        printDoubleArrayToConsole(cycle.getPrices());
        System.out.println();
        System.out.println();

        System.out.println("Стоимость модели '4x4': " + automobile.getPrice("4x4"));
        System.out.println("Изменяем стоимость модели '4x4' на 888");
        automobile.setPrice("4x4", 888);
        System.out.println("Новая тоимость модели '4x4': " + automobile.getPrice("4x4"));
        System.out.println();

        System.out.println("Изменям название модели 'kalina' на 'audi'");
        automobile.setModel("kalina", "audi");
        System.out.println("Вывод информации по маркам");
        printStringArrayToConsole(automobile.getNames());
        System.out.println();

        System.out.println("--------");
        printPeaople();
        System.out.println("--------");

        Transport.showList(cycle);
        System.out.println("Средняя цена: " + Transport.getAvg(cycle));
        System.out.println();
        Transport.showList(automobile);
        System.out.println("Средняя цена: " + Transport.getAvg(automobile));
    }

    public static void printPeaople() {
        System.out.println("   **  ");
        System.out.println("   **  ");
        System.out.println("********");
        System.out.println("*  **  *");
        System.out.println("*  **  *");
        System.out.println("  ****  ");
        System.out.println("  *  *  ");
        System.out.println(" *    * ");
        System.out.println("**    **");
    }

    public static void printStringArrayToConsole(String[] array) {
        for (String item : array) {
            System.out.print(item + ", ");
        }
    }

    public static void printDoubleArrayToConsole(double[] array) {
        for (double item : array) {
            System.out.print(item + ", ");
        }
    }

}
