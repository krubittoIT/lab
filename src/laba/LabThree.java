package laba;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import Services.Transport;
import contracts.ITransport;
import entity.Automobile;
import errors.DuplicateModelNameException;
import errors.NoSuchModelNameException;

public class LabThree {

    public static void doLadThreeSerObj()
            throws DuplicateModelNameException, NoSuchModelNameException, IOException, ClassNotFoundException {

        Automobile automobile = new Automobile("lada", 4);
        Transport.showList(automobile);
        // сохранение состояния экземпляра автомобили
        FileOutputStream fos = new FileOutputStream("C://Users//Виктория//Desktop//java//lab3//temp.out");
        ObjectOutputStream oos = new ObjectOutputStream(fos);// Для сериализации объектов в поток
        oos.writeObject(automobile);// записывает в поток отдельный объект
        oos.close();// закрывает поток
        fos.close();
        // воссоздания объекта из файла
        FileInputStream fis = new FileInputStream("C://Users//Виктория//Desktop//java//lab3//temp.out");
        ObjectInputStream oin = new ObjectInputStream(fis);
        ITransport ts = (Automobile) oin.readObject();
        oin.close();

        ts.add("4x4", 200);
        System.out.println("Вывод после воссоздания и добовления новой модели");
        System.out.println("модели и прайс лист:");
        Transport.showList(ts);
    }

    public static void doLabThreThread() throws DuplicateModelNameException, NoSuchModelNameException, IOException {

        System.out.print("---Запись и чтение из байтового потока---");
        System.out.println();
        Automobile automobile = new Automobile("lada", 4);
        System.out.print("Создана модель для передачи по байтовому/символьному потокам:");
        Transport.showList(automobile);

        // Запись информации о марке автомобиля и моделях в байтовый поток
        System.out.println("Происходит запись в байтовый поток");
        try {
            FileOutputStream fout = new FileOutputStream("C://Users//Виктория//Desktop//java//lab3//Byte.txt");
            Transport.outputTransport(automobile, fout);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        // Чтение информации о марке автомобиля и моделях из байтового потока
        System.out.println("Происходит чтение из байтового потока");
        try {
            FileInputStream finp = new FileInputStream("C://Users//Виктория//Desktop//java//lab3//Byte.txt");
            System.out.println("Размер файла: " + finp.available() + " байт(а)");
            ITransport byte_p1 = Transport.inputTransport(finp);
            System.out.println(byte_p1.getMark());
            System.out.print("Модели и цены:");
            System.out.println();
            Transport.showList(byte_p1);
            System.out.println();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        System.out.print("---Запись и чтение из символьного потока---");
        System.out.println();

        // Запись информации о марке автомобиля и моделях в символьный поток
        try {
            System.out.println("Происходит запись в символьный поток");
            FileWriter fwrite = new FileWriter("C://Users//Виктория//Desktop//java//lab3//Symb.txt");
            Transport.writeTransport(automobile, fwrite);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        // Чтение информации о марке автомобиля и моделях из символьного потока
        System.out.println("Происходит чтение из символьного потока");
        try {
            FileReader fread = new FileReader("C://Users//Виктория//Desktop//java//lab3//Symb.txt");
            ITransport symb_p1 = Transport.readTransport(fread);
            System.out.println(symb_p1.getMark());
            System.out.print("Модели и цены:");
            System.out.println();
            Transport.showList(symb_p1);
            System.out.println();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        System.out.print("---Запись и чтение через стандартные---");
        System.out.println();

        // стандартный(реальные) выходной поток байты в симнов
        Writer k1 = new OutputStreamWriter(System.out);
        // стандартный входной поток тарнсляция между символьно и байт потоками
        Reader k2 = new InputStreamReader(System.in);
        System.out.println("Ввод описания транспорта:");
        ITransport p = Transport.readTransport(k2);
        System.out.println("Вывод описания транспорта:");
        Transport.writeTransport(p, k1);

    }

}
