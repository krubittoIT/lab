package laba;

import contracts.ITransport;
import entity.Automobile;
import entity.Motorcycle;
import errors.DuplicateModelNameException;
import errors.NoSuchModelNameException;

public class LabFour {

    public static void doLadFour()
            throws DuplicateModelNameException, CloneNotSupportedException, NoSuchModelNameException {

        Motorcycle cycleOne = new Motorcycle("Henday", 4);
        cycleOne.add("vesta", 50);
        ITransport cycleTwo = cycleOne;
        ITransport automobileThree = new Automobile("Audi", 1);
        Automobile automobileFour = new Automobile("Lada", 2);
        automobileFour.add("xray", 100);
        ITransport automobileFive = automobileFour;

        System.out.println("Хеш код:");
        System.out.println("1-й транспорт : " + cycleOne + " и его хеш код: " + cycleOne.hashCode());
        System.out.println("2-й транспорт : " + cycleTwo + " и его хеш код: " + cycleTwo.hashCode());
        System.out.println("3-й транспорт : " + automobileThree + " и его хеш код: " + automobileThree.hashCode());
        System.out.println("4-й транспорт : " + automobileFour + " и его хеш код: " + automobileFour.hashCode());
        System.out.println("5-й транспорт : " + automobileFive + " и его хеш код: " + automobileFive.hashCode());

        System.out.println("Клонирование:");
        Motorcycle clonedCycleOne = (Motorcycle) cycleOne.clone();
        Automobile clonedAutomobileFour = (Automobile) automobileFour.clone();

        clonedAutomobileFour.setModel("xray", "newName");
        System.out.println(automobileFour + " и его хеш код: " + automobileFour.hashCode());
        System.out.println(clonedAutomobileFour + " и его хеш код: " + clonedAutomobileFour.hashCode());

        // clonedCycleOne.setMark("nMark");
        clonedCycleOne.setPrice("vesta", 6);
        clonedCycleOne.setModel("vesta", "nName");
        // clonedCycleOne.delete("vesta", 6);
        // cycleOne.delete("none-2", 2);
        clonedCycleOne.delete("Henday-0", 0);
        clonedCycleOne.delete("nName", 6);
        // clonedCycleOne.add("куку", 69);
        System.out.println(cycleOne + " и его хеш код: " + cycleOne.hashCode());
        System.out.println(clonedCycleOne + " и его хеш код: " + clonedCycleOne.hashCode());

        System.out.println("оригинал (1-й) : " + cycleOne + " и его хеш код: " + cycleOne.hashCode());
        System.out.println("копия : " + clonedCycleOne + " и его хеш код: " + clonedCycleOne.hashCode());
        System.out.println("оригинал (4-й) : " + automobileFour + " и его хеш код: " + automobileFour.hashCode());
        System.out.println("копия : " + clonedAutomobileFour + " и его хеш код: " + clonedAutomobileFour.hashCode());

        System.out.println("Сравнение:");
        System.out.println("1-й транспорт и 2-й транспорт (такой же) " + cycleOne.equals(cycleTwo));
        System.out.println("1-й транспорт и клонированный " + cycleOne.equals(clonedCycleOne));
        System.out.println("1-й транспорт и 3-й транспорт " + cycleOne.equals(automobileThree));

        System.out.println("5-й транспорт и 4-й транспорт (такой же) " + automobileFive.equals(automobileFour));
        System.out.println("4-й транспорт и клонированный " + clonedAutomobileFour.equals(clonedAutomobileFour));
        System.out.println("5-й транспорт и 3-й транспорт " + automobileFive.equals(automobileThree));
    }

}
