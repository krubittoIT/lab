package laba;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import Services.Transport;
import contracts.ITransport;
import entity.Atv;
import entity.Automobile;
import entity.Moped;
import entity.Scooter;
import errors.DuplicateModelNameException;
import errors.NoSuchModelNameException;

public class LabFive {

    public static void doLabFive(String[] args) throws DuplicateModelNameException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException,
            SecurityException, NoSuchModelNameException, InstantiationException, IOException {

        Automobile automobile = new Automobile("Lada", 2);
        automobile.add("xray", 100);
        automobile.setPrice("none-1", 100.10);
        System.out.println(automobile.toString());

        // Задание 1
        System.out.println();
        System.out.println("Рефлексия - Automobile setPrice none-0 10.10");
        var className = args[0];
        var methodName = args[1];
        var modelName = args[2];
        var newPrice = Double.valueOf(args[3]);
        Class refClass = Class.forName(className);
        Method m = refClass.getMethod(methodName, String.class, double.class);
        m.invoke(automobile, modelName, newPrice);
        System.out.println(automobile.toString());

        // Задание 2
        System.out.println();
        System.out.println("Создание транспортного средства");
        ITransport transport = Transport.createTransport("fnew", 2, automobile);
        transport.add("xray", 100);
        transport.setPrice("none-1", 100.10);
        transport.setModel("none-0", "new-none");
        System.out.println(transport.toString());
        System.out.println(transport.getClass());

        // Задание 3
        System.out.println();
        System.out.println("Скутер");
        Scooter scooter = new Scooter("honda", 4);
        scooter.setMark("new-mark");
        scooter.add("xray", 100);
        scooter.delete("honda-1", 1.0);

        System.out.println("Цены:");
        var prices = scooter.getPrices();
        for (double d : prices)
            System.out.print(d + " ");
        System.out.println();

        System.out.println("Модели:");
        var models = scooter.getNames();
        for (String model : models)
            System.out.print(model + " ");
        System.out.println();

        System.out.println(scooter.toString());
        scooter.setPrice("honda-3", 55.55);
        scooter.setModel("xray", "new-xray");
        System.out.println(scooter.toString());

        // Задание 4
        System.out.println();
        System.out.println("Квадроцикл");
        Atv atv = new Atv("honda", 4);
        atv.setMark("new-new-mark");
        atv.add("xray-ray", 100.152);
        atv.delete("honda-1", 1.0);

        System.out.println("Цены:");
        var atvPrices = atv.getPrices();
        for (double d : atvPrices)
            System.out.print(d + " ");
        System.out.println();

        System.out.println("Модели:");
        var atvModels = atv.getNames();
        for (String model : atvModels)
            System.out.print(model + " ");
        System.out.println();

        System.out.println(atv.toString());
        atv.setPrice("honda-3", 252.525);
        atv.setModel("xray-ray", "xray-ray-new");
        System.out.println(atv.toString());

        // Задание 5
        System.out.println();
        System.out.println("Мопед");
        Moped moped = new Moped("honda", 4);
        moped.setMark("new-mark-new-neeeew");
        moped.add("ra-xray-vega", 999.586);
        moped.delete("honda-1", 1.0);

        System.out.println("Цены:");
        var mopedPrices = moped.getPrices();
        for (double d : mopedPrices)
            System.out.print(d + " ");
        System.out.println();

        System.out.println("Модели:");
        var mopedModels = moped.getNames();
        for (String model : mopedModels)
            System.out.print(model + " ");
        System.out.println();

        System.out.println(moped.toString());
        moped.setPrice("honda-3", 641.146);
        moped.setModel("ra-xray-vega", "ra-xray-vega-new");
        System.out.println(moped.toString());

        // Задание 6
        System.out.println();
        System.out.println("Новое среднее арефметическое");
        System.out.println(moped.toString());
        System.out.println(atv.toString());
        System.out.println(Transport.getAvgPer(moped, atv));

        // Задание 7
        System.out.println();
        System.out.println("Новые чтение и запись:");
        System.out.println("Заись в файл:");
        moped.setModel("honda-2", "honda-2-new");
        moped.setPrice("none-2-new", 66.66);
        moped.setModel("honda-3", "honda-none-3-new");
        System.out.println(moped.toString());
        File criteFile = new File("newVvod.txt");
        criteFile.createNewFile();
        Writer writerFile = new FileWriter(criteFile);
        Transport.writeTransportPrintf(moped, writerFile);
        writerFile.close();

        System.out.println("Чтение из файла:");
        Reader readerFile = new FileReader(criteFile);
        ITransport readedTransport = Transport.readTransportScanner(readerFile);
        System.out.println(readedTransport.toString());
        readerFile.close();
    }

}
