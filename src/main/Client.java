package main;

import java.io.*;
import java.net.Socket;
import contracts.ITransport;
import entity.Automobile;
import errors.DuplicateModelNameException;
//import threads.RunnableThread.HttpSendThread;

public class Client {
    public static void main(String[] args) throws IOException, DuplicateModelNameException {
        ITransport autoOne = new Automobile("lada", 2);
        ITransport autoTwo = new Automobile("audi", 3);
        ITransport[] transports = new ITransport[] { autoOne, autoTwo };
        ObjectOutputStream out = null;
        ObjectInputStream in = null;
        Socket conn = null;
        try {
            conn = new Socket("localhost", 8888);
            System.out.println("Соединение установлено");
            out = new ObjectOutputStream(conn.getOutputStream());
            in = new ObjectInputStream(conn.getInputStream());
            out.writeObject(transports);
            System.out.println("Среднее значение оценок: " + in.readDouble());
        } finally {
            out.flush();
            conn.close();
            in.close();
        }
    }
}
