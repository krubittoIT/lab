package main;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import Services.Transport;
import contracts.ITransport;
import threads.RunnableThread.HttpWorkThread;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Server {
    public static void main(String[] args) {
        try {
            ServerSocket server = new ServerSocket(8888);
            //startServer(server);
            startServerParalel(server);
        } catch (IOException e) {
            System.out.println("Could not listen on port: 8888");
            System.exit(-1);
        }
    }
    private static void startServer(ServerSocket server) throws IOException {
        try {
            System.out.println("Сервер запущен");
            while (true) {
                Socket client = server.accept();
                System.out.println("Соединение установлено");
                ObjectOutputStream out = new ObjectOutputStream(client.getOutputStream());
                ObjectInputStream in = new ObjectInputStream(client.getInputStream());
                ITransport[] transports = (ITransport[]) in.readObject();
                // ShowReciveTransport(transports);
                out.writeDouble(Transport.getAvgPer(transports));
                out.flush();
                System.out.println("Значение передано клиенту");
                client.close();
                System.out.println("Обработан клиент");
            }

        } catch (Exception e) {
            System.out.println(e);
            server.close();
            System.exit(-1);

        }
        finally{
            server.close();
        }
    }

    private static void startServerParalel(ServerSocket server) throws IOException {
        try {
            System.out.println("Сервер запущен");
            while (true) {
                Socket client = server.accept();
                Runnable rn = new HttpWorkThread(client);
                Thread tr = new Thread(rn);
                tr.start();
            }
        }
         catch (Exception e) {
            System.out.println(e);
            server.close();
            System.exit(-1);
        }
        finally{
            server.close();
        }
    }
    

    private static void ShowReciveTransport(ITransport[] transports) {
        System.out.println("Сервер получил:");
        for (ITransport transport : transports) {
            Transport.showList(transport);
        }
    }
}