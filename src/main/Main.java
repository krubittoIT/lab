package main;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import errors.DuplicateModelNameException;
import errors.NoSuchModelNameException;

public class Main {
    public static void main(String[] args)
            throws NoSuchModelNameException, DuplicateModelNameException, ClassNotFoundException, IOException,
            CloneNotSupportedException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
            NoSuchMethodException, SecurityException, InstantiationException, InterruptedException {

        // LabTwo.doLabTwo();
        // LabThree.doLadThreeSerObj();
        // LabThree.doLabThreThread();
        // LabFour.doLadFour();
        // LabFive.doLabFive(args);

        // LabSix.taskOne();
        // LabSix.taskTwo();
        // LabSix.taskThree();
        // LabSix.taskFour();
        // LabSix.taskFive();

    }
}