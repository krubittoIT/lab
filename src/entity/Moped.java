package entity;

import java.util.LinkedList;
import contracts.ITransport;
import errors.DuplicateModelNameException;
import errors.ModelPriceOutOfBoundsException;
import errors.NoSuchModelNameException;

public class Moped implements ITransport {
    private String mark = "<Без имени>";
    private LinkedList<Model> models = new LinkedList();

    public Moped(String mark, int countOfModels) {
        this.mark = mark;
        LinkedList<Model> new_models = new LinkedList<>();
        for (int i = 0; i < countOfModels; i++) {
            new_models.add(new Model(mark + "-" + i, i));
        }
        this.models = new_models;
    }

    private class Model {
        private String name;
        private double price;

        Model(String name, double price) {
            this.name = name;
            this.price = price;
        }
    }

    @Override
    public String getMark() {
        return mark;
    }

    @Override
    public void setMark(String mark) {
        this.mark = mark;

    }

    @Override
    public void setModel(String oldName, String newName) throws NoSuchModelNameException {
        boolean isDone = false;
        for (var entity : models) {
            if (entity.name.equals(oldName))
                entity.name = newName;
            isDone = true;
        }
        if (!isDone) {
            throw new NoSuchModelNameException(oldName);
        }
    }

    @Override
    public String[] getNames() {
        String[] name_models = new String[models.size()];
        for (int i = 0; i < models.size(); i++)
            name_models[i] = models.get(i).name;
        return name_models;
    }

    @Override
    public double getPrice(String name) throws NoSuchModelNameException {
        for (var entity : models) {
            if (entity.name.equals(name))
                return entity.price;
        }
        throw new NoSuchModelNameException(name);
    }

    @Override
    public void setPrice(String name, double newPrice) throws NoSuchModelNameException {
        boolean isDone = false;
        if (newPrice < 0) {
            throw new ModelPriceOutOfBoundsException();
        }
        for (var entity : models) {
            if (entity.name.equals(name))
                entity.price = newPrice;
            isDone = true;
        }
        if (!isDone) {
            throw new NoSuchModelNameException(name);
        }
    }

    @Override
    public double[] getPrices() {
        double[] price_models = new double[models.size()];
        for (int i = 0; i < models.size(); i++)
            price_models[i] = models.get(i).price;
        return price_models;
    }

    @Override
    public void add(String name, double price) throws DuplicateModelNameException {
        for (var entity : models) {
            if (entity.name.equals(name))
                throw new DuplicateModelNameException(name);
        }
        if (price < 0)
            throw new ModelPriceOutOfBoundsException();
        models.add(new Model(name, price));
    }

    @Override
    public void delete(String name, double price) throws NoSuchModelNameException {
        boolean isDone = false;
        for (int i = 0; i < models.size(); i++) {
            if (models.get(i).name.equals(name) && models.get(i).price == price) {
                models.remove(i);
                isDone = true;
            }
        }
        if (!isDone) {
            throw new NoSuchModelNameException(name);
        }
    }

    @Override
    public int getLength() {
        return models.size();
    }

    public String toString() {
        StringBuffer sb = new StringBuffer("Марка: " + mark + " | " + "модель - цена: ");
        for (var entity : models) {
            sb.append(entity.name);
            sb.append(" - ");
            sb.append(entity.price + ", ");
        }
        return sb.toString();
    }

}
