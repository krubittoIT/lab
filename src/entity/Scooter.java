package entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import contracts.ITransport;
import errors.DuplicateModelNameException;
import errors.ModelPriceOutOfBoundsException;
import errors.NoSuchModelNameException;

public class Scooter implements ITransport {
    private String mark = "<Без имени>";
    private HashMap<String, Double> models = new HashMap<>();

    public Scooter(String mark, int countOfModels) {
        this.mark = mark;
        HashMap<String, Double> new_models = new HashMap<>();
        for (int i = 0; i < countOfModels; i++) {
            new_models.put(mark + "-" + i, (double) i);
        }
        this.models = new_models;

    }

    @Override
    public String getMark() {
        return mark;
    }

    @Override
    public void setMark(String mark) {
        this.mark = mark;
    }

    @Override
    public void setModel(String oldName, String newName) throws NoSuchModelNameException {
        if (models.containsKey(oldName)) {
            var oldPrice = models.get(oldName);
            models.remove(oldName);
            models.put(newName, oldPrice);
        } else {
            throw new NoSuchModelNameException(oldName);
        }
    }

    @Override
    public String[] getNames() {
        String[] allModel = new String[models.size()];
        ArrayList<String> modelsName = new ArrayList<>(models.keySet());
        for (int i = 0; i < modelsName.size(); i++)
            allModel[i] = modelsName.get(i);
        return allModel;
    }

    @Override
    public double getPrice(String name) throws NoSuchModelNameException {
        if (models.containsKey(name)) {
            return models.get(name);
        } else {
            throw new NoSuchModelNameException(name);
        }
    }

    @Override
    public void setPrice(String name, double newPrice) throws NoSuchModelNameException {
        if (newPrice < 0) {
            throw new ModelPriceOutOfBoundsException();
        }
        if (models.containsKey(name)) {
            models.put(name, newPrice);
        } else {
            throw new NoSuchModelNameException(name);
        }
    }

    @Override
    public double[] getPrices() {
        double[] allPrices = new double[models.size()];
        ArrayList<Double> modelsPrice = new ArrayList<>(models.values());
        for (int i = 0; i < modelsPrice.size(); i++)
            allPrices[i] = modelsPrice.get(i);
        return allPrices;
    }

    @Override
    public void add(String name, double price) throws DuplicateModelNameException {
        if (price < 0) {
            throw new ModelPriceOutOfBoundsException();
        }
        if (models.containsKey(name)) {
            throw new DuplicateModelNameException(name);
        } else {
            models.put(name, price);
        }
    }

    @Override
    public void delete(String name, double price) throws NoSuchModelNameException {
        if (models.containsKey(name)) {
            double oldPrice = models.get(name);
            if (oldPrice == price) {
                models.remove(name);
            } else {
                throw new NoSuchModelNameException(name);
            }
        } else {
            throw new NoSuchModelNameException(name);
        }
    }

    @Override
    public int getLength() {
        return models.size();
    }

    public String toString() {
        StringBuffer sb = new StringBuffer("Марка: " + mark + " | " + "модель - цена: ");
        for (Map.Entry<String, Double> entity : models.entrySet()) { //возвращает объект Set, элементами которого служат объекты типа Map.Entry, представляющие отдельные пары соответствий текущей коллекции.
            sb.append(entity.getKey());
            sb.append(" - ");
            sb.append(entity.getValue() + ", ");
        }
        return sb.toString();
    }

}
