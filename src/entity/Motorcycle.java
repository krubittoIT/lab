package entity;

import java.io.Serializable;
import java.util.Arrays;
import contracts.ITransport;
import errors.DuplicateModelNameException;
import errors.ModelPriceOutOfBoundsException;
import errors.NoSuchModelNameException;

public class Motorcycle implements ITransport, Serializable, Cloneable {
    private String mark = "<Без имени>";

    private Model head = new Model(0.0, "none");
    {
        head.prev = head;
        head.next = head;

    };
    private int size = 0;

    private class Model implements Serializable {
        String name = null;
        double price = Double.NaN;
        Model prev = null;
        Model next = null;

        public Model(double d, String name) {
            this.price = d;
            this.name = name;
        }

        public Model(Double price, String name, Model n, Model p) {
            this.price = price;
            this.name = name;
            this.next = n;
            this.prev = p;
        }

    }

    // далее код по заданию

    // метод для получения марки мотоцикла
    public String getMark() {
        return mark;
    }

    // метод для модификации марки мотоцикла
    public void setMark(String mark) {
        this.mark = mark;
    }

    // метод для модификации значения названия модели,
    public void setModel(String oldName, String newName) throws NoSuchModelNameException {
        if (head != null) {
            {
                Model p = this.head.next;
                while (p != head && !p.name.equals(oldName)) {
                    p = p.next;
                }
                if (p != head) {
                    p.name = newName;
                } else {
                    throw new NoSuchModelNameException(oldName);
                }
            }
        }
    }

    // метод, возвращающий массив названий всех моделей,
    public String[] getNames() {
        String[] name_models = new String[this.size];
        int i = 0;
        if (head != null) {
            Model p = head.next;
            while (p != head) {
                name_models[i] = p.name;
                p = p.next;
                i++;
            }
        }
        return name_models;
    }

    // метод для получения значения цены модели по её названию,
    public double getPrice(String name) throws NoSuchModelNameException {
        if (head != null) {
            {
                Model p = head.next;
                while (p != head && !p.name.equals(name)) {
                    p = p.next;
                }
                if (p != head) {
                    return p.price;
                } else {
                    throw new NoSuchModelNameException(name);
                }
            }
        }
        return 0;
    }

    // метод для модификации значения цены модели по её названию,
    public void setPrice(String name, double newPrice) throws NoSuchModelNameException {
        if (newPrice < 0) {
            throw new ModelPriceOutOfBoundsException();
        }
        if (head != null) {
            {
                Model p = head.next;
                while (p != head && !p.name.equals(name)) {
                    p = p.next;
                }
                if (p != head) {
                    p.price = newPrice;
                } else {
                    throw new NoSuchModelNameException(name);
                }
            }
        }

    }

    // метод, возвращающий массив значений цен моделей,
    public double[] getPrices() {
        double[] price_models = new double[this.size];
        int i = 0;
        if (head != null) {
            Model p = head.next;
            while (p != head) {
                price_models[i] = p.price;
                p = p.next;
                i++;
            }
        }
        return price_models;
    }

    // метод добавления названия модели и её цены
    // добавление узла в конец
    public void add(String nameModel, double price) throws DuplicateModelNameException {
        if (head != null) {
            Model p = head.next;
            while (p != head && p.name != nameModel) {
                p = p.next;
            }
            if (p.name == nameModel)
                throw new DuplicateModelNameException(nameModel);
        }
        if (price < 0) {
            throw new ModelPriceOutOfBoundsException();
        }
        if (head != null) {
            Model p = new Model(price, nameModel, head, head.prev);
            p.prev.next = p;
            head.prev = p;
            this.size++;
        }
    }

    // метод удаления модели с заданным именем и её цены, использовать методы
    public void delete(String name, double price) throws NoSuchModelNameException {
        if (head != null) {
            {
                Model p = head.next;
                while (p != head && !p.name.equals(name) && p.price != price) {
                    p = p.next;
                }
                if (p != head) {
                    p.next.prev = p.prev;
                    p.prev.next = p.next;

                    p.next = null;
                    p.prev = null;
                } else {
                    throw new NoSuchModelNameException(name);
                }
                int counter = 0;
                if (head != null) {
                    p = head.next;
                    while (p != head) {
                        p = p.next;
                        counter++;
                    }
                    size = counter;
                }
            }
        }

    }

    // метод для получения размера массива Моделей.
    public int getLength() {
        int counter = 0;
        if (head != null) {
            Model p = head.next;
            while (p != head) {
                p = p.next;
                counter++;
            }
        }
        return counter;
        // return this.size;
    }

    /*
     * Конструктор класса должен принимать в качестве параметров значение Марки
     * автомобиля и размер массива Моделей.
     */
    public Motorcycle(String mark, int countOfModels) throws DuplicateModelNameException {
        this.mark = mark;
        for (int i = 0; i < countOfModels; i++) {
            this.add(mark + "-" + i, i);
        }
    }

    public String toString() {
        StringBuffer sb = new StringBuffer("Марка: " + mark + " | " + "модель - цена: ");

        String[] names = getNames();
        double[] prices = getPrices();
        for (int i = 0; i < names.length; i++) {
            sb.append("" + names[i]);
            sb.append(" = ");
            sb.append(prices[i] + ", ");
        }
        return sb.toString();
    }

    public int hashCode() {
        int result = mark != null ? mark.hashCode() : 0;

        String[] names = getNames();
        double[] prices = getPrices();

        result = 31 * result + (names != null ? Arrays.hashCode(names) : 0);
        result = 31 * result + (prices != null ? Arrays.hashCode(prices) : 0);

        return result;
    }

    public Object clone() throws CloneNotSupportedException {
        Motorcycle clon = (Motorcycle) super.clone();
        Model p = this.head.next;
        Model clonedHead = new Model(0, "");
        clonedHead.prev = clonedHead;
        clonedHead.next = clonedHead;
        while (p != this.head) {
            Model cloneP = new Model(0, "");
            cloneP.name = p.name;
            cloneP.price = p.price;
            cloneP.prev = clonedHead.prev;
            cloneP.next = clonedHead;
            clonedHead.prev.next = cloneP;
            clonedHead.prev = cloneP;
            p = p.next;
        }
        clon.head = clonedHead;
        return clon;
    }

    public boolean equals(Object obj) {
        boolean flag = false;
        int k = 0;
        if (obj == this)
            flag = true;
        else {

            if (obj instanceof Motorcycle) {
                Motorcycle temp = (Motorcycle) obj;
                if (temp.getMark().equals(mark) && (temp.getLength() == getLength())) {
                    String[] tempModelsNames = temp.getNames();
                    double[] tempModelsPrices = temp.getPrices();
                    String[] modelsNames = getNames();
                    double[] modelsPrices = getPrices();
                    for (int i = 0; i < getLength(); i++)
                        if (modelsPrices[i] == tempModelsPrices[i] && tempModelsNames[i].equals(modelsNames[i]))
                            k++;
                } else
                    return false;
            } else
                return false;
            if (k == getLength()) {
                flag = true;
            } else {
                flag = false;
            }
        }
        return flag;
    }

}
