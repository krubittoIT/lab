package entity;

import java.io.Serializable;
import java.util.Arrays;
import contracts.ITransport;
import errors.DuplicateModelNameException;
import errors.ModelPriceOutOfBoundsException;
import errors.NoSuchModelNameException;

public class Automobile implements ITransport, Serializable, Cloneable {
    // поле типа String, хранящее марку автомобиля
    private String mark = "<Без имени>";
    private Model[] models;

    // метод для получения марки автомобиля
    public String getMark() {
        return mark;
    }

    // метод для модификации марки автомобиля
    public void setMark(String mark) {
        this.mark = mark;
    }

    /*
     * внутренний класс Модель, имеющий поля название модели и её цену, а также
     * конструктор (класс Автомобиль хранит массив Моделей)
     */
    private class Model implements Serializable, Cloneable {
        private String name;
        private double price;

        Model(String name, double price) {
            this.name = name;
            this.price = price;
        }

        public Object clone() throws CloneNotSupportedException {
            Model clon = (Model) super.clone();
            return clon;
        }
    }

    // метод для модификации значения названия модели,
    public void setModel(String oldName, String newName) throws NoSuchModelNameException {
        boolean isDone = false;
        for (int i = 0; i < models.length; i++) {
            if (models[i].name.equals(oldName))
                models[i].name = newName;
            isDone = true;
        }
        if (!isDone) {
            throw new NoSuchModelNameException(oldName);
        }
    }

    // метод, возвращающий массив названий всех моделей,
    public String[] getNames() {
        String[] name_models = new String[models.length];
        for (int i = 0; i < models.length; i++)
            name_models[i] = models[i].name;
        return name_models;
    }

    // метод для получения значения цены модели по её названию,
    public double getPrice(String name) throws NoSuchModelNameException {
        for (int i = 0; i < models.length; i++) {
            if (models[i].name == name)
                return models[i].price;
        }
        throw new NoSuchModelNameException(name);
    }

    // метод для модификации значения цены модели по её названию,
    public void setPrice(String name, double newPrice) throws NoSuchModelNameException {
        boolean isDone = false;
        if (newPrice < 0) {
            throw new ModelPriceOutOfBoundsException();
        }
        for (int i = 0; i < models.length; i++) {
            if (models[i].name.equals(name))
                models[i].price = newPrice;
            isDone = true;
        }
        if (!isDone) {
            throw new NoSuchModelNameException(name);
        }
    }

    // метод, возвращающий массив значений цен моделей,
    public double[] getPrices() {
        double[] price_models = new double[models.length];
        for (int i = 0; i < models.length; i++)
            price_models[i] = models[i].price;
        return price_models;
    }

    // метод добавления названия модели и её цены (путем создания нового массива
    // Моделей), использовать метод Arrays.copyOf(),
    public void add(String name, double price) throws DuplicateModelNameException {
        for (int i = 0; i < models.length; i++) {
            if (models[i].name == name) {
                throw new DuplicateModelNameException(name);
            }
        }
        if (price < 0) {
            throw new ModelPriceOutOfBoundsException();
        }
        models = Arrays.copyOf(models, models.length + 1);
        models[models.length - 1] = new Model(name, price);
    }

    // метод удаления модели с заданным именем и её цены, использовать методы
    // System.arraycopy, Arrays.copyOf(),
    public void delete(String name, double price) throws NoSuchModelNameException {
        boolean isDone = false;
        for (int i = 0; i < models.length; i++) {
            if (models[i].name.equals(name) && models[i].price == price) {
                System.arraycopy(models, i + 1, models, i, models.length - i - 1);
                models = Arrays.copyOf(models, models.length - 1);
                isDone = true;
            }
        }
        if (!isDone) {
            throw new NoSuchModelNameException(name);
        }
    }

    // метод для получения размера массива Моделей
    public int getLength() {
        return models.length;
    }

    /*
     * Конструктор класса должен принимать в качестве параметров значение Марки
     * автомобиля и размер массива Моделей.
     */
    public Automobile(String mark, int countOfModels) {
        this.mark = mark;
        Model[] new_models = new Model[countOfModels];
        for (int i = 0; i < countOfModels; i++) {
            new_models[i] = new Model(mark + "-" + i, i);
        }
        this.models = new_models;

    }

    public String toString() {
        StringBuffer sb = new StringBuffer("Марка: " + mark + " | " + "модель - цена: ");
        for (int i = 0; i < models.length; i++) {
            sb.append("" + models[i].name);
            sb.append(" - ");
            sb.append(models[i].price + ", ");
        }
        return sb.toString();
    }

    public int hashCode() {
        int result = mark != null ? mark.hashCode() : 0;
        result = 31 * result + (models != null ? Arrays.hashCode(models) : 0);
        return result;
    }

    public Object clone() throws CloneNotSupportedException {
        Automobile clon = (Automobile) super.clone();
        clon.models = models.clone();
        for (int i = 0; i < models.length; i++) {
            clon.models[i] = (Model) models[i].clone();
        }
        return clon;
    }

    public boolean equals(Object obj) {
        boolean flag = false;
        int k = 0;
        if (obj == this)
            flag = true;
        else {

            if (obj instanceof Automobile) {
                Automobile temp = (Automobile) obj;
                if (temp.getMark().equals(mark) && (temp.getLength() == getLength())) {
                    String[] tempModelsNames = temp.getNames();
                    double[] tempModelsPrices = temp.getPrices();
                    String[] modelsNames = getNames();
                    double[] modelsPrices = getPrices();
                    for (int i = 0; i < getLength(); i++)
                        if (modelsPrices[i] == tempModelsPrices[i] && tempModelsNames[i].equals(modelsNames[i]))
                            k++;
                } else
                    return false;
            } else
                return false;
            if (k == getLength()) {
                flag = true;
            } else {
                flag = false;
            }
        }
        return flag;
    }

}
