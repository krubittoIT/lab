package calculator;

import java.awt.*;
import java.awt.event.*;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CalculatorBuilder {

    // #region
    private JTextField inputTextField;
    private JTextField historyTextField;

    private JButton button0;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JButton button7;
    private JButton button8;
    private JButton button9;
    private JButton buttonDot;
    private JButton buttonAdd;
    private JButton buttonSub;
    private JButton buttonMult;
    private JButton buttonDiv;
    private JButton buttonRes;
    private JButton buttonC;
    private JButton buttonSqrt;
    private JButton buttonPow;

    private double resultValue = 0;
    private double newValue = 0;
    private String resultExpression = "";
    private String lastSymbol = "";

    private boolean checkError = false;
    private boolean isResult = false;
    private boolean afterOperButton = false;
    private boolean afterSqrt = false;

    // #endregion

    public void Build() {
        JFrame frame = new JFrame("калькулятор");

        BorderLayout borderLayout = new BorderLayout();
        frame.setLayout(borderLayout);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(300, 350));

        JPanel panelButton = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JPanel panelField = new JPanel();
        panelField.setLayout(new BoxLayout(panelField, BoxLayout.Y_AXIS));
        panelField.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        frame.add(panelField, BorderLayout.NORTH);
        setHostoryTextField(panelField);
        setInputTextField(panelField);

        frame.add(panelButton, BorderLayout.CENTER);
        setButtons(panelButton);
        setButtonAction();

        setKeyEvent();
        inputTextField.requestFocus();

        frame.setResizable(false);
        frame.pack();
        frame.setVisible(true);

        inputTextField.requestFocus();
    }

    private void setInputTextField(JPanel panel) {
        inputTextField = new JTextField("");
        inputTextField.setFont(new Font("Times New Roman", Font.BOLD, 16));
        inputTextField.setHorizontalAlignment(JTextField.RIGHT);
        inputTextField.setEditable(false);

        panel.add(inputTextField);
    }

    private void setHostoryTextField(JPanel panel) {
        historyTextField = new JTextField("");
        historyTextField.setFont(new Font("Dialog", Font.PLAIN, 14));
        historyTextField.setHorizontalAlignment(JTextField.RIGHT);
        historyTextField.setEditable(false);

        panel.add(historyTextField);

    }

    private void setButtons(JPanel panel) {

        button7 = new JButton("7");
        button7.setPreferredSize(new Dimension(50, 50));
        button8 = new JButton("8");
        button8.setPreferredSize(new Dimension(50, 50));
        button9 = new JButton("9");
        button9.setPreferredSize(new Dimension(50, 50));
        panel.add(button7);
        panel.add(button8);
        panel.add(button9);

        buttonMult = new JButton("*");
        buttonMult.setPreferredSize(new Dimension(50, 50));
        buttonDiv = new JButton("/");
        buttonDiv.setPreferredSize(new Dimension(50, 50));
        panel.add(buttonMult);
        panel.add(buttonDiv);

        button4 = new JButton("4");
        button4.setPreferredSize(new Dimension(50, 50));
        button5 = new JButton("5");
        button5.setPreferredSize(new Dimension(50, 50));
        button6 = new JButton("6");
        button6.setPreferredSize(new Dimension(50, 50));
        panel.add(button4);
        panel.add(button5);
        panel.add(button6);

        buttonAdd = new JButton("+");
        buttonAdd.setPreferredSize(new Dimension(50, 50));
        buttonSub = new JButton("-");
        buttonSub.setPreferredSize(new Dimension(50, 50));
        panel.add(buttonAdd);
        panel.add(buttonSub);

        button1 = new JButton("1");
        button1.setPreferredSize(new Dimension(50, 50));
        button2 = new JButton("2");
        button2.setPreferredSize(new Dimension(50, 50));
        button3 = new JButton("3");
        button3.setPreferredSize(new Dimension(50, 50));
        panel.add(button1);
        panel.add(button2);
        panel.add(button3);

        buttonSqrt = new JButton("√");
        buttonSqrt.setPreferredSize(new Dimension(50, 50));
        buttonPow = new JButton("^");
        buttonPow.setPreferredSize(new Dimension(50, 50));
        panel.add(buttonSqrt);
        panel.add(buttonPow);

        button0 = new JButton("0");
        button0.setPreferredSize(new Dimension(50, 50));
        buttonDot = new JButton(".");
        buttonDot.setPreferredSize(new Dimension(50, 50));
        panel.add(button0);
        panel.add(buttonDot);

        buttonC = new JButton("C");
        buttonC.setPreferredSize(new Dimension(50, 50));
        buttonRes = new JButton("=");
        buttonRes.setPreferredSize(new Dimension(100, 50));
        panel.add(buttonC);
        panel.add(buttonRes);
    }

    private void setButtonAction() {
        setActionForNumberButton(button0, "0");
        setActionForNumberButton(button1, "1");
        setActionForNumberButton(button2, "2");
        setActionForNumberButton(button3, "3");
        setActionForNumberButton(button4, "4");
        setActionForNumberButton(button5, "5");
        setActionForNumberButton(button6, "6");
        setActionForNumberButton(button7, "7");
        setActionForNumberButton(button8, "8");
        setActionForNumberButton(button9, "9");

        setActionForOperationButton(buttonAdd, MathOperation.Add);
        setActionForOperationButton(buttonSub, MathOperation.Sub);
        setActionForOperationButton(buttonMult, MathOperation.Mult);
        setActionForOperationButton(buttonDiv, MathOperation.Div);
        setActionForOperationButton(buttonPow, MathOperation.Pow);
        setActionForOperationButton(buttonSqrt, MathOperation.Sqrt);

        buttonC.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearAll();
            }
        });

        buttonDot.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!checkError && !isResult) {
                    var valueInInputField = inputTextField.getText();
                    if (valueInInputField.length() > 0) {
                        if (!valueInInputField.contains(".")) {
                            inputTextField.setText(inputTextField.getText() + '.');
                            inputTextField.requestFocus();
                        }
                    }
                }
                inputTextField.requestFocus();
            }
        });

        buttonRes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!checkError && !resultExpression.equals("")) {
                    if (!inputTextField.getText().equals("")) {
                        var valueInInputField = Double.parseDouble(inputTextField.getText());
                        var valueForOperation = isResult || afterSqrt ? newValue : valueInInputField;
                        if (checkErrorOperation(valueForOperation, lastSymbol)) {
                            switch (lastSymbol) {
                                case "+": {
                                    resultValue = resultValue + valueForOperation;
                                    break;
                                }
                                case "-": {
                                    resultValue = resultValue - valueForOperation;
                                    break;
                                }
                                case "/": {
                                    resultValue = resultValue / valueForOperation;
                                    break;
                                }
                                case "*": {
                                    resultValue = resultValue * valueForOperation;
                                    break;
                                }
                                case "^": {
                                    resultValue = Math.pow(resultValue, valueForOperation);
                                    break;
                                }
                                case "√": {
                                    resultValue = Math.sqrt(resultValue);
                                    break;
                                }
                            }
                        }
                    }
                    if (!checkError) {
                        if (isResult || afterSqrt) {
                            removeLastSymbolFromResultExpression("=");
                            if (lastSymbol.equals("√")) {
                                setResultExpression(lastSymbol, inputTextField.getText() + "=");
                            } else {
                                setResultExpression(lastSymbol, newValue + "=");
                            }
                        } else {
                            newValue = Double.parseDouble(inputTextField.getText());
                            if (lastSymbol.equals("√")) {
                                setResultExpression("√" + newValue, "=");
                            } else {
                                setResultExpression(inputTextField.getText(), "=");
                            }
                        }
                        inputTextField.setText(resultValue + "");
                        isResult = true;
                        inputTextField.requestFocus();
                    }
                }
                inputTextField.requestFocus();
            }
        });
    }

    private void setActionForNumberButton(JButton button, String number) {
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isResult) {
                    isResult = false;
                    clearAll();
                }
                if (checkError) {
                    clearAll();
                    checkError = false;
                }
                if (afterOperButton) {
                    inputTextField.setText("");
                    afterOperButton = false;
                }
                if (afterSqrt) {
                    clearAll();
                    afterSqrt = false;
                }

                var valueInInputField = inputTextField.getText();
                if (number.equals("0")) {
                    if (valueInInputField.length() == 1 && valueInInputField.equals("0")) {
                    } else {
                        inputTextField.setText(valueInInputField + number);
                    }
                } else {
                    if (valueInInputField.length() == 1 && valueInInputField.equals("0")) {
                        inputTextField.setText(number);
                    } else {
                        inputTextField.setText(valueInInputField + number);
                    }

                }
                inputTextField.requestFocus();
            }
        });
    }

    private void setActionForOperationButton(JButton button, MathOperation type) {
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isResult) {
                    historyTextField.setText("");
                    lastSymbol = "";
                    isResult = false;
                }
                if (!checkError) {
                    var valueInInputField = Double.parseDouble(inputTextField.getText());
                    if (lastSymbol.equals("")) {
                        newValue = valueInInputField;
                        switch (type) {
                            case Add: {
                                resultValue = valueInInputField;
                                setResultExpression(inputTextField.getText(), "+");
                                lastSymbol = "+";
                                break;
                            }
                            case Sub: {
                                resultValue = valueInInputField;
                                setResultExpression(inputTextField.getText(), "-");
                                lastSymbol = "-";
                                break;
                            }
                            case Mult: {
                                resultValue = valueInInputField;
                                setResultExpression(inputTextField.getText(), "*");
                                lastSymbol = "*";
                                break;
                            }
                            case Div: {
                                resultValue = valueInInputField;
                                setResultExpression(inputTextField.getText(), "/");
                                lastSymbol = "/";
                                break;
                            }
                            case Pow: {
                                resultValue = resultExpression.isEmpty() ? valueInInputField
                                        : Math.pow(resultValue, valueInInputField);
                                setResultExpression(inputTextField.getText(), "^");
                                lastSymbol = "^";
                                break;
                            }
                            case Sqrt: {
                                if (checkErrorOperation(newValue, "√")) {
                                    resultValue = Math.sqrt(newValue);
                                    setResultExpression("√", inputTextField.getText());
                                    lastSymbol = "√";
                                    afterSqrt = true;
                                }
                                break;
                            }
                        }
                    } else {
                        if (type == MathOperation.Sqrt) {
                            if (checkErrorOperation(valueInInputField, "√")) {
                                setResultExpression("√", valueInInputField + "");
                                newValue = Math.sqrt(valueInInputField);

                                if (!afterSqrt) {
                                    afterSqrt = true;
                                    switch (lastSymbol) {
                                        case "+": {
                                            resultValue = resultValue + newValue;
                                            break;
                                        }
                                        case "-": {
                                            resultValue = resultValue - newValue;
                                            break;
                                        }
                                        case "/": {
                                            resultValue = resultValue / newValue;
                                            break;
                                        }
                                        case "*": {
                                            resultValue = resultValue * newValue;
                                            break;
                                        }
                                        case "^": {
                                            resultValue = Math.pow(resultValue, newValue);
                                            break;
                                        }
                                        case "√": {
                                            resultValue = Math.sqrt(resultValue);
                                            break;
                                        }
                                    }
                                } else {
                                    resultValue = newValue;
                                }
                            }
                        } else {
                            if (afterSqrt) {
                                afterSqrt = false;
                                switch (type) {
                                    case Add: {
                                        setResultExpression("", "+");
                                        lastSymbol = "+";
                                        break;
                                    }
                                    case Sub: {
                                        setResultExpression("", "-");
                                        lastSymbol = "-";
                                        break;
                                    }
                                    case Mult: {
                                        setResultExpression("", "*");
                                        lastSymbol = "*";
                                        break;
                                    }
                                    case Div: {
                                        setResultExpression("", "/");
                                        lastSymbol = "/";
                                        break;
                                    }
                                    case Pow: {
                                        setResultExpression("", "^");
                                        lastSymbol = "^";
                                        break;
                                    }
                                }
                            } else {
                                if (!afterOperButton) {
                                    newValue = valueInInputField;
                                    switch (lastSymbol) {
                                        case "+": {
                                            resultValue = resultValue + newValue;
                                            break;
                                        }
                                        case "-": {
                                            resultValue = resultValue - newValue;
                                            break;
                                        }
                                        case "/": {
                                            resultValue = resultValue / newValue;
                                            break;
                                        }
                                        case "*": {
                                            resultValue = resultValue * newValue;
                                            break;
                                        }
                                        case "^": {
                                            resultValue = Math.pow(resultValue, newValue);
                                            break;
                                        }
                                    }

                                    switch (type) {
                                        case Add: {
                                            setResultExpression(newValue + "", "+");
                                            lastSymbol = "+";
                                            break;
                                        }
                                        case Sub: {
                                            setResultExpression(newValue + "", "-");
                                            lastSymbol = "-";
                                            break;
                                        }
                                        case Mult: {
                                            setResultExpression(newValue + "", "*");
                                            lastSymbol = "*";
                                            break;
                                        }
                                        case Div: {
                                            setResultExpression(newValue + "", "/");
                                            lastSymbol = "/";
                                            break;
                                        }
                                        case Pow: {
                                            setResultExpression(newValue + "", "^");
                                            lastSymbol = "^";
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!checkError) {
                        afterOperButton = true;
                        inputTextField.setText(resultValue + "");
                        inputTextField.requestFocus();
                    }
                }
                inputTextField.requestFocus();
            }
        });
    }

    public void setKeyEvent() {
        inputTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_NUMPAD0:
                        button0.doClick();
                        break;
                    case KeyEvent.VK_NUMPAD1:
                        button1.doClick();
                        break;
                    case KeyEvent.VK_NUMPAD2:
                        button2.doClick();
                        break;
                    case KeyEvent.VK_NUMPAD3:
                        button3.doClick();
                        break;
                    case KeyEvent.VK_NUMPAD4:
                        button4.doClick();
                        break;
                    case KeyEvent.VK_NUMPAD5:
                        button5.doClick();
                        break;
                    case KeyEvent.VK_NUMPAD6:
                        button6.doClick();
                        break;
                    case KeyEvent.VK_NUMPAD7:
                        button7.doClick();
                        break;
                    case KeyEvent.VK_NUMPAD8:
                        button8.doClick();
                        break;
                    case KeyEvent.VK_NUMPAD9:
                        button9.doClick();
                        break;
                    case KeyEvent.VK_DECIMAL:
                        buttonDot.doClick();
                        break;
                    case KeyEvent.VK_DIVIDE:
                        buttonDiv.doClick();
                        break;
                    case KeyEvent.VK_MULTIPLY:
                        buttonMult.doClick();
                        break;
                    case KeyEvent.VK_SUBTRACT:
                        buttonSub.doClick();
                        break;
                    case KeyEvent.VK_ADD:
                        buttonAdd.doClick();
                        break;
                    case KeyEvent.VK_ENTER:
                        buttonRes.doClick();
                        break;
                    case KeyEvent.VK_DELETE:
                        buttonC.doClick();
                        break;
                    // case KeyEvent.VK_BACK_SPACE:
                    // buttonBek.doClick();
                    default:

                }
            }

        });

    }

    private void clearAll() {
        inputTextField.setText("");
        historyTextField.setText("");
        resultExpression = "";
        lastSymbol = "";
        resultValue = 0;
        newValue = 0;
        checkError = false;
        isResult = false;
        afterOperButton = false;
        afterSqrt = false;
        inputTextField.requestFocus();
    }

    private void removeLastSymbolFromResultExpression(String symbol) {
        var index = resultExpression.indexOf(symbol);
        if (index != -1) {
            var newLine = resultExpression.substring(0, index);
            historyTextField.setText(newLine);
            resultExpression = newLine;
        }
    }

    private void setResultExpression(String newText, String oper) {
        historyTextField.setText(historyTextField.getText() + newText + oper);
        resultExpression = resultExpression + newText + oper;
    }

    private boolean checkErrorOperation(Double valueForOperation, String symbol) {
        switch (symbol) {
            case "/": {
                if (valueForOperation == 0) {
                    inputTextField.setText("делить на 0 нелья");
                    checkError = true;
                    return false;
                }
            }
            case "√": {
                if (valueForOperation < 0) {
                    inputTextField.setText("корень из числа < 0");
                    checkError = true;
                    return false;
                }
            }
        }
        return true;
    }

}
