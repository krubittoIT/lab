package threads.BasicThread;

import contracts.ITransport;

public class PriceThread extends Thread {
    private ITransport transport;

    public PriceThread(ITransport tr) {
        transport = tr;
    }

    @Override
    public void run() {
        var prices = transport.getPrices();
        for (double price : prices) {
            System.out.println("PriceThread-" + "Цена: " + price);
        }
    }
}
