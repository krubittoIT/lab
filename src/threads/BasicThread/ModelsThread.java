package threads.BasicThread;

import contracts.ITransport;

public class ModelsThread extends Thread {
    private ITransport transport;

    public ModelsThread(ITransport tr) {
        transport = tr;
    }

    @Override
    public void run() {
        var models = transport.getNames();
        for (String model : models) {
            System.out.println("ModelsThread-" + "Модель: " + model);
        }
    }
}
