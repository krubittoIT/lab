package threads.ExecutorThread;

import contracts.ITransport;

public class ExecutRunnable implements Runnable {
    private ITransport transport;

    public ExecutRunnable(ITransport tr) {
        transport = tr;
    }

    @Override
    public void run() {
        System.out.println("Sync- " + transport.getMark());
    }

}
