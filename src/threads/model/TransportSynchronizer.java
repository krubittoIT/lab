package threads.model;

import contracts.ITransport;

public class TransportSynchronizer {
    private ITransport v;
    private volatile int current = 0;
    private Object lock = new Object();
    private boolean set = false;
   
    public TransportSynchronizer(ITransport v) {
        this.v = v;
    }
   
    public double printPrice() throws InterruptedException {
        double val;
        synchronized(lock) {
            double [] p = v.getPrices();
            if (!canPrintPrice()) throw new InterruptedException();
            while (!set)
                lock.wait();
            val = p[current++];
            System.out.println("Print price: " + val);
            set = false;
            lock.notifyAll();
        }
        return val;
    }  
   
    public void printModel() throws InterruptedException {
        synchronized(lock) {
            String [] s = v.getNames();
            if (!canPrintModel()) throw new InterruptedException();
            while (set)
                lock.wait();
            System.out.println("Print model: " + s[current]);
            set = true;
            lock.notifyAll();
        }
    }
    
    public boolean canPrintPrice() {
        return current < v.getLength();
    }
    
    public boolean canPrintModel() {
        return (!set && current < v.getLength()) || (set && current < v.getLength() - 1);
    }


}
