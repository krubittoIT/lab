package threads.RunnableLockThread;

import java.util.concurrent.locks.ReentrantLock;
import contracts.ITransport;

public class PriceRunnableLock implements Runnable {
    private ITransport transport;
    private ReentrantLock rlook;

    public PriceRunnableLock(ITransport tr, ReentrantLock rl) {
        transport = tr;
        rlook = rl;
    }

    @Override
    public void run() {
        rlook.lock();
        try {            
            var prices = transport.getPrices();
            for (double price : prices) {
                System.out.println("PriceRunnableLock-" + "Цена: " + price);
            }
        } finally {
            rlook.unlock();
        }
    }

}
