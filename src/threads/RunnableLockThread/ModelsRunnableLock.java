package threads.RunnableLockThread;

import java.util.concurrent.locks.ReentrantLock;
import contracts.ITransport;

public class ModelsRunnableLock implements Runnable {
    private ITransport transport;
    private ReentrantLock rlook;

    public ModelsRunnableLock(ITransport tr, ReentrantLock rl) {
        transport = tr;
        rlook = rl;
    }

    @Override
    public void run() {
       rlook.lock();
        try {            
            var models = transport.getNames();
            for (String model : models) {
                System.out.println("ModelsRunnableLock-" + "Модель: " + model);
            }
        } 
        finally {
            rlook.unlock();
        }
    }

}
