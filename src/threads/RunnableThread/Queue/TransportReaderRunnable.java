package threads.RunnableThread.Queue;

import java.util.concurrent.ArrayBlockingQueue;

import contracts.ITransport;

public class TransportReaderRunnable implements Runnable {
    ArrayBlockingQueue<ITransport> blockingQueue;

    public TransportReaderRunnable(ArrayBlockingQueue<ITransport> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        try {
            while (true) {
                ITransport ff = blockingQueue.take();
                System.out.println(ff.toString());
            }
        } catch (Exception e) {
            System.out.println("error:" + e.getMessage());
        }
    }
}
