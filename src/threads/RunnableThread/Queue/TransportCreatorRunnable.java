package threads.RunnableThread.Queue;

import java.io.FileReader;
import java.io.Reader;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;
import contracts.ITransport;
import entity.Automobile;

public class TransportCreatorRunnable implements Runnable {
    String fileName;
    ArrayBlockingQueue<ITransport> blockingQueue;

    public TransportCreatorRunnable(String fileName, ArrayBlockingQueue<ITransport> blockingQueue) {
        this.fileName = fileName;
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        try {
            Reader readerFile = new FileReader(fileName);
            Scanner sc = new Scanner(readerFile);
            var mark = sc.nextLine();
            ITransport transport = new Automobile(mark, 1);
            //blockingQueue.put(transport);
            blockingQueue.add(transport);
        } catch (Exception e) {
            System.out.println("error:" + e.getMessage());
        }
    }
}
