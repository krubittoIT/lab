package threads.RunnableThread;

import java.net.Socket;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import contracts.ITransport;

public class HttpSendThread implements Runnable {
    private int index;
    private ITransport[] transients;

    public HttpSendThread(int i, ITransport[] t) {
        index = i;
        transients = t;
    }

    @Override
    public void run() {
        try {
            int serverPort = 8888;
            String address = "localhost";
            Socket conn = new Socket(address, serverPort);
            System.out.println("Соединение установлено");

            ObjectOutputStream out = new ObjectOutputStream(conn.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(conn.getInputStream());
            out.writeInt(index);
            out.writeObject(transients);
            out.flush();
            System.out.println("Среднее значение оценок: " + in.readDouble());
            conn.close();
            in.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
