package threads.RunnableThread;

import java.net.Socket;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import contracts.ITransport;
import Services.Transport;

public class HttpWorkThread implements Runnable {
    private Socket client;

    public HttpWorkThread(Socket s) {
        client = s;
    }

    @Override
    public void run() {
        try {
            System.out.println("Соединение установлено");

            ObjectOutputStream out = new ObjectOutputStream(client.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(client.getInputStream());
            ITransport[] transports = (ITransport[]) in.readObject();
            // ShowReciveTransport(transports);
            out.writeDouble(Transport.getAvgPer(transports));
            out.flush();
            System.out.println("Значение передано клиенту");
            client.close();
            System.out.println("Обработан клиент");
        } catch (IOException ex) {
            System.err.println(ex);
        } catch (ClassNotFoundException ex) {
            System.err.println(ex);
        }
    }

    private void ShowReciveTransport(ITransport[] transports) {
        System.out.println("Сервер получил:");
        for (ITransport transport : transports) {
            Transport.showList(transport);
        }
    }

}
