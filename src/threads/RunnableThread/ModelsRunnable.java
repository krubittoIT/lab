package threads.RunnableThread;

import threads.model.TransportSynchronizer;

public class ModelsRunnable implements Runnable {
    private TransportSynchronizer transportSynchronizer;

    public ModelsRunnable(TransportSynchronizer ts) {
        transportSynchronizer = ts;
    }

    @Override
    public void run() {
        try {
            while (transportSynchronizer.canPrintModel())
                transportSynchronizer.printModel();
        } catch (InterruptedException ex) {
        }
    }
}
