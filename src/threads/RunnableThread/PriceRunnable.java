package threads.RunnableThread;

import threads.model.TransportSynchronizer;

public class PriceRunnable implements Runnable {
    private TransportSynchronizer transportSynchronizer;

    public PriceRunnable(TransportSynchronizer ts) {
        transportSynchronizer = ts;
    }

    @Override
    public void run() {
        try {
            while (transportSynchronizer.canPrintPrice())
                transportSynchronizer.printPrice();
        } catch (InterruptedException ex) {
        }
    }
}
